@extends('layouts.app')

@section('content')
    <h2>Product </h2>
    <a href="{{url('/products')}}">Product List</a>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        {!! Form::open(['url'=>'/products','role'=>'form','method'=>'POST','files'=>true]) !!}
                            @include('product.form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
