<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    public function index()
    {
        $id = Auth::user()->id;
        $products = Product::where('supplier_id',$id)->paginate(5);
        return view('product.index',compact('products'));
    }
    public function create()
    {
        $categories = Category::all();
        $data =['categories'=>$categories];
        return view('product.create',compact('data'));
    }
    public function store(Request $request)
    {
        $requestData = $request->except('image');
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time().'.'. $image->getClientOriginalExtension();
            $path = 'img/';
            if(!File::exists($path)) File::makeDirectory($path, 775);
            $location = ($path.$filename);
            Image::make($image)->resize(650,525)->save($location);
            $requestData['image'] = $location;
        }
        Product::create($requestData);
        return redirect()->back();
    }
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $product = Product::findorfail($id);
        $categories = Category::all();

        $data = [
            'product'=>$product,
            'categories'=>$categories,
        ];
        return view('product.edit',compact('data'));
    }
    public function update(Request $request, $id)
    {
        $requestData = $request->except('image');

        $product = Product::findorfail($id);
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time().'.'. $image->getClientOriginalExtension();
            $path = 'img/';
            if(!File::exists($path)) File::makeDirectory($path, 775);
            $location = ($path.$filename);
            Image::make($image)->resize(650,525)->save($location);
            $requestData['image'] = $location;
        }
        $product->update($requestData);
        return redirect()->back();
    }

    public function destroy($id)
    {
        Product::destroy($id);
        return redirect()->back();
    }
}
