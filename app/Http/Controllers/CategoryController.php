<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = category::OrderBy('id', 'desc')->paginate(2);
        return view('category.index',['categories'=>$categories]);
    }
    public function create()
    {
        return view('category.create');
    }
    public function store(Request $request)
    {
        $requestData = $request->all();
            if (empty($request->status)){
                $requestData['status'] = 1;
            }
        Category::create($requestData);
        return redirect()->back();
    }
    public function show($id,Category $category)
    {
    }
    public function edit($id)
    {
        $data = Category::find($id);
        return view('category.edit',compact('data'));
    }
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        $category = Category::findorFail($id);
        $category->update($requestData);

        return redirect('/categories');
    }
    public function destroy($id)
    {
        category::destroy($id);
        return redirect('/categories');
    }
}
